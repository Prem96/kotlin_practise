package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.SeekBar
import java.util.*




class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollbutton = findViewById<Button>(R.id.button_good)
        val results = findViewById<TextView>(R.id.results)
        val seekBar = findViewById<SeekBar>(R.id.seekBar_good)

        rollbutton.setOnClickListener {
            val rand = Random().nextInt(seekBar.progress)
            results.text = rand.toString()

        }
    }
}
